package org.mpierce.ktor

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import io.ktor.sessions.SessionStorageMemory
import io.ktor.sessions.Sessions
import io.ktor.sessions.cookie
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class KtorUseEntropyTest {
    private val appInit: Application.() -> Unit = {
        install(Routing)

        install(Sessions) {
            cookie<CookieData>("session", storage = SessionStorageMemory())
        }

        routing {
            get("/cookie/{name}") {
                val name = call.parameters["name"]!!

                call.sessions.set(CookieData(name))

                call.respond(HttpStatusCode.OK)
            }
        }

    }

    @Test
    internal fun exceptionFromRouteHandlerWorks() {
        withTestApplication(appInit) {
            (1..1000).map { counter ->
                with(handleRequest(HttpMethod.Get, "/cookie/$counter")) {
                    assertEquals(HttpStatusCode.OK, response.status())
                }
                println("Finished $counter")
            }
        }
    }
}

data class CookieData(val name: String)
