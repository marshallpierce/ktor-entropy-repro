On Linux, Ktor's nonce generation quickly exhausts entropy.
